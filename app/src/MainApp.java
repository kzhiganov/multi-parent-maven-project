import com.mifmif.common.regex.Generex;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

@Controller
@EnableAutoConfiguration
public class MainApp {
	public static void main(String[] args) throws Throwable {
		Generex generex = new Generex("[0-3]([a-c]|[e-g]{1,2})");
		// Generate random String
		String randomStr = generex.random();
		System.out.println(randomStr);

	}

	@RequestMapping("/")
	@ResponseBody
	String home() {
		return "Hello World!";
	}
}
